package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginMorethen6Regular() {
		assertTrue("Correct Login", LoginValidator.isValidLoginName("bilaalrashid"));
	}
	
	@Test
	public void testIsValidLoginMorethen6Excption() {
		assertFalse("Invalid Login", LoginValidator.isValidLoginName("b"));
	}
	
	@Test
	public void testIsValidLoginMorethen6BoundaryIn() {
		assertTrue("Correct Login", LoginValidator.isValidLoginName("bilaal"));
	}

	@Test
	public void testIsValidLoginMorethen6BoundaryOut() {
		assertFalse("Invalid Login", LoginValidator.isValidLoginName("bilaa"));
	}
	
	@Test
	public void testIsValidLoginHasCharacterNumberRegular() {
		assertTrue("Correct Login", LoginValidator.isValidLoginName("bilaal123"));
	}
	
	@Test
	public void testIsValidLoginHasCharacterNumberExcption() {
		assertFalse("Invalid Login", LoginValidator.isValidLoginName("!@#$"));
	}
	
	@Test
	public void testIsValidLoginHasCharacterNumberBoundaryIn() {
		assertTrue("Correct Login", LoginValidator.isValidLoginName("bilaa1"));
	}
	
	@Test
	public void testIsValidLoginHasCharacterNumberBoundaryOut() {
		assertFalse("Invalid Login", LoginValidator.isValidLoginName("bila1"));
	}
}
