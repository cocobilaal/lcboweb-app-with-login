package sheridan;

import java.util.regex.Pattern;

public class LoginValidator {

	public static boolean isValidLoginName(String loginName) {

		Pattern letterAndDigit = Pattern.compile("^[a-zA-Z0-9_.-]*$");

		boolean check = true;

		if (loginName.length() < 6) {
			check = false;
		}
		if (!letterAndDigit.matcher(loginName).find()) {
			check = false;
		}

		return check;
	}
}
